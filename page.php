<?php while (have_posts()) : the_post(); ?>
  <div class="row">
    <div class="col-12 col-md-8">
      <?php get_template_part('templates/page', 'header'); ?>
      <?php get_template_part('templates/content', 'page'); ?>
    </div>
    <?php get_template_part('templates/contact-form', 'page'); ?>
  </div>
<?php endwhile; ?>
