<footer id="footer">
<div class="blank-left">
  </div>
    <div class="container">
      <div id="footerSocial" class="grey-bg">
        <div class="blank-top">
        </div>
        <div id="social">
          <div class="social-left">
            <div id="socialHolder">
              <a id="facebookLink" href="#"><img class="social-icon" src="<?php echo get_template_directory_uri(); ?>/assets/images/fb-icon.png" /></a>
              <a id="instragramLink" href="#"><img class="social-icon"src="<?php echo get_template_directory_uri(); ?>/assets/images/inst-icon.png" /></a>
            </div>
            <div id="cRightHolder">
              <p>
                &copy; <?php echo date("Y"); echo " "; echo bloginfo('name'); ?>
              </p>
            </div>
          </div>
          <div id="logoContainer" class="">
            <?php 
            if ( function_exists( 'the_custom_logo' ) ) {
                the_custom_logo();
            } ?>
          </div>
        </div>
       
      </div>
      <div id="footerContact" class="orange-bg">
        <div class="blank-top">
        </div>
        <div class="contact-holder">
        <div class="blank">
        </div>
          <div id="contactInfo" >
            <p>Call: 07586 321 4896</p>
            <p>Email: enquiries@msmdrivingsch.co.uk</p>
          </div>
        </div>
      </div>
    </div>
  <div class="blank-right">
  </div>
</footer>
