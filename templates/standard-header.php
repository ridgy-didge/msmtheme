<div id="contentHolder" >
  <div id="contentInner" >
    <?php get_template_part('templates/page', 'header'); ?>
    <?php get_template_part('templates/content', 'page'); ?>
    
    <div id="mainFieldsContainer">
      <?php 
        $rows = get_field('bullet_points');
        if($rows)
        {
          foreach($rows as $row)
          {
            ?>
            <div class="bullet-point-holder">
              <img class="bullet-image" src="<?php echo get_template_directory_uri(); ?>/assets/images/l-plate.png">
              <p class="col"><?php echo  $row['bullet_point']; ?></p>
            </div>
          <?php  } ?>
        <?php  } ?>
    </div>
  </div>
</div>
