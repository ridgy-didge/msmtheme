<header id="header" class="banner">
  <div class="blank-left">
  </div>
  <div class="container">
    <div id="logoContainer" class="orange-bg ">
      <div id="logoHolder" class="">
        <?php 
        if ( function_exists( 'the_custom_logo' ) ) {
            the_custom_logo();
        } ?>
      </div>
    </div >
    <div id="mainNavContainer" class="grey-bg">
      
      <nav class="nav-primary">
        <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'main-menu']);
        endif;
        ?>
      </nav>
    </div>
  </div>
  <div class="blank-right">
  </div>
</header>


