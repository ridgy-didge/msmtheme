<?php if( have_rows('prices') ): ?>
    <div id="prices">
    <?php 
    // loop through rows (parent repeater)
    while( have_rows('prices') ): the_row(); ?>
    <?php // var_dump('prices') ?>

      <?php if( have_rows('packages') ): ?>

        <?php while( have_rows('packages') ): the_row(); ?>

          <?php if( have_rows('package_group') ): ?>

            <?php while( have_rows('package_group') ): the_row(); ?>

              <div id="pricesContainer" class="container-fluid orange-bg">
                <div id="" class="container">
                
                  <div class="price-type">

                    <div class="price-type-title">
                      <h3><?php the_sub_field('package_group_title'); ?></h3>
                    </div>
                    <div class="price-type-title">
                      <p><?php the_sub_field('package_description'); ?></p>
                    </div>
                    <div class="price-headers-md">
                      <div class="blank">
                      </div>
                      <div class="price-header-holder">
                        <div class="price-header">
                          <p>Price £</p>
                        </div>
                        <div class="discount-header">
                          <p>Discount %</p>
                        </div>
                        <div class="orginal-header">
                          <p>Orginal Price £</p>
                        </div>
                      </div>
                    </div>

                    <?php 
                    // check for rows (sub repeater)
                    if( have_rows('package') ): ?>
                      
                      <?php while( have_rows('package') ): the_row();  ?>
                        <div class="price-container">
                          <div class="package-desc">
                          <p><?php the_sub_field('package_description'); ?></p>
                          </div>
                          
                            <div class="price-headers">
                              <div class="price-header">
                                <p>Price £</p>
                              </div>
                              <div class="discount-header">
                                <p>Discount %</p>
                              </div>
                              <div class="orginal-header">
                                <p>Orginal Price £</p>
                              </div>
                            </div>
                            <div class="prices">
                              <div class="package-price"><p><?php the_sub_field('package_price'); ?></p></div>
                              <div class="package-discount"><p><?php the_sub_field('package_discount'); ?></p></div>
                              <div class="package-orgPrice"><p><?php the_sub_field('package_original_price'); ?></p></div>
                            </div>
                          
                        </div>
                      <?php endwhile; ?>
                    <?php endif; //if( get_sub_field('package') ): ?>
                  </div>	
                </div>	
              </div>	
            <?php endwhile; ?>
          <?php endif; //if( get_sub_field('package_group') ): ?>
        <?php endwhile; ?>
      <?php endif; //if( get_sub_field('packages') ): ?>
    <?php endwhile; ?>
    </div>
<?php endif; // if( get_field('to-prices') ): ?>