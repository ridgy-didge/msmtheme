<div id="map" class="orange-bg">
  <div class="container">
    <?php
    $areaTitle = get_field( "google_maps_area_title" );
    $areaDesc = get_field( "google_maps_area_text" );

    if( $areaTitle ) { ?>
        
      <h3><?php echo $areaTitle; ?></h3>

    <?php } ?>
    
    <?php if( $areaDesc ) { ?>
        
      <p> <?php echo $areaDesc;?></p>

    <?php } ?>
  </div>
  <div id="iframeHolder">
    <iframe src="https://www.google.com/maps/d/embed?mid=1xhAT3rjNVaneK8yuzOTqe5NazBuYYnle" ></iframe>
  </div>
</div>