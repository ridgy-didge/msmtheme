<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>



    <?php if ( is_page_template( 'template-home.php' ) ) { ?>
      <div id="topContainer" >
        <div id="main-wrap" class="wrap" role="document">
          <div class="container">
            <main class="main">
              <?php include Wrapper\template_path(); ?>
            </main><!-- /.main -->
          </div><!-- /.container -->
        </div><!-- /.wrap -->
      </div><!-- /#topContainer -->
    <?php } ?><!-- / endif -->

    <?php if ( is_page_template( 'template-prices.php' ) ) { ?>
      <div id="topContainer" >
        <div id="main-wrap" class="wrap" role="document">
          <div class="container">
            <main class="main">
              <?php include Wrapper\template_path(); ?>
            </main><!-- /.main -->
          </div><!-- /.container -->
        </div><!-- /.wrap -->
      </div><!-- /#topContainer -->
     <?php get_template_part('templates/prices');
    } ?><!-- / endif -->

    <?php if ( is_page_template( 'template-areas-covered.php' ) ) { ?>      
      <div id="topContainer" >
        <div id="main-wrap" class="wrap" role="document">
          <div class="container">
            <main class="main">
              <?php include Wrapper\template_path(); ?>
            </main><!-- /.main -->
          </div><!-- /.container -->
        </div><!-- /.wrap -->
      </div><!-- /#topContainer -->
      <?php get_template_part('templates/google-maps'); ?>
      
    <?php } ?><!-- / endif -->


    <div id="toTop">
      <a href="#header"> <img src="<?php echo get_template_directory_uri(); ?>/dist/images/arrow-up.png" width="" height="" alt="" /> </a>
    </div>

    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>

  </body>
</html>
