<?php
/**
 * Template Name: Homepage Template
 */
?>

<?php while (have_posts()) : the_post(); ?>

    <div id="contentHolder-xs" class="" >
      <?php get_template_part('templates/page', 'header'); ?>
      <div class="content-holder"><?php get_template_part('templates/content', 'page'); ?></div>
    </div>

    <div id="contentHolder" class="" >
        <div id="carouselIndicators" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
              <?php get_template_part('templates/page', 'header'); ?>
              <div class="content-holder"><?php get_template_part('templates/content', 'page'); ?></div>
              <div class="d-block w-100" id="mainFieldsContainer">
                <?php 
                  $rows = get_field('bullet_points');
                  if($rows)
                  {
                    foreach($rows as $row)
                    {
                      ?>
                      <div class="bullet-point-holder">
                        <img class="bullet-image" src="<?php echo get_template_directory_uri(); ?>/assets/images/l-plate.png">
                        <p class="col"><?php echo  $row['bullet_point']; ?></p>
                      </div>
                    <?php  } ?>
                <?php  } ?>
              </div>
              
            </div>
            <div class="carousel-item">
              <?php 
                $post = get_post( 4 ); 
                $title = $post->post_title;
                $slug = $post->post_name;
                $link = get_permalink( );
              ?>
              <?php get_template_part('templates/page', 'header'); ?>
              <?php // fetch page content ?>
              <div class="content-holder"><?php get_template_part('templates/content', 'page'); ?></div>
              <div class="d-block w-100" id="mainFieldsContainer">
                <?php 
                  $rows = get_field('bullet_points', $post );
                  if($rows)
                  {
                    foreach($rows as $row)
                    {
                      ?>
                      <div class="bullet-point-holder">
                        <img class="bullet-image" src="<?php echo get_template_directory_uri(); ?>/assets/images/l-plate.png">
                        <p class=""><?php echo  $row['bullet_point']; ?></p>
                      </div>
                    <?php  } ?>
                    <?php wp_reset_query(); ?>
                <?php  } ?>
              </div>
              <div class="slider-page-link">
                <a href="<?php echo $link; ?>"><?php echo $title ?></a>
              </div>
            </div>
            <div class="carousel-item">
              <?php 
               $post = get_post( 40 ); 
               $title = $post->post_title;
               $slug = $post->post_name;
               $link = get_permalink( );
              ?>
              <?php get_template_part('templates/page', 'header'); ?>
              <div class="content-holder"><?php get_template_part('templates/content', 'page'); ?></div>
              <div class="d-block w-100" id="mainFieldsContainer">
                <?php 
                  $rows = get_field('bullet_points',  $post  );
                  if($rows)
                  {
                    foreach($rows as $row)
                    {
                      ?>
                      <div class="bullet-point-holder">
                        <img class="bullet-image"src="<?php echo get_template_directory_uri(); ?>/assets/images/l-plate.png">
                        <p class=""><?php echo  $row['bullet_point']; ?></p>
                      </div>
                    <?php  } ?>                      
                  <?php wp_reset_query(); ?>
                <?php  } ?>
              </div>
              <div class="slider-page-link">
                <a href="<?php echo $link; ?>"><?php echo $title ?></a>
              </div>
            </div>
          <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>


        <div id="carouselIndicatorHolder" class="">
        <div class="carousel-indicators">
          <div class="active carousel-indicator" data-target="#carouselIndicators" data-slide-to="0" >
            <div class="carousel-icon-holder">
                <?php $image = get_field('slider_button_image'); ?>
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
            </div>
            <div class="carousel-text-holder">
                <p class=""><?php echo  get_field('slider_button_text'); ?></p>
            </div>
          </div>
          <div class="carousel-indicator" data-target="#carouselIndicators" data-slide-to="1">
            <?php
              $post = get_post( 4 ); 
            ?>
              <div class="carousel-icon-holder">
                  <?php $image = get_field('slider_button_image'); ?>
                  <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
              </div>
              <div class="carousel-text-holder">
                  <p class=""><?php echo  get_field('slider_button_text'); ?></p>
              </div>
            <?php wp_reset_query(); ?>
          </div>
          <div class="carousel-indicator" data-target="#carouselIndicators" data-slide-to="2">
            <?php
              $post = get_post( 40 ); 
            ?>
              <div class="carousel-icon-holder">
                <?php $image = get_field('slider_button_image'); ?>
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
              </div>
              <div class="carousel-text-holder">
                  <p class=""><?php echo  get_field('slider_button_text'); ?></p>
              </div>
            <?php wp_reset_query(); ?>
          </div>
        </div>
      </div>


     
      </div>
    </div>
    <?php get_template_part('templates/contact-form', 'page'); ?>
<?php endwhile; ?>
